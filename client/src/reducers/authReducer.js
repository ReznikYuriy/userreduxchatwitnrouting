import {
    LOGIN_SUCCESSFUL,
    ADMIN_LOGGED_IN
  } from "../actions/actionTypes";
  
  const initialState = {
      isLogin:false,
      isAdmin:false,
      currentUser:{}
  };
  const authReducer = (state = initialState, action) => {
    switch (action.type) {
      case LOGIN_SUCCESSFUL:
        return {
          ...state,
          isLogin:true,
          currentUser:action.user
        };
      case ADMIN_LOGGED_IN:
        return {
          ...state,
          isAdmin:true,
        };
      
      default:
        return state;
    }
  };
  
  export default authReducer;
  