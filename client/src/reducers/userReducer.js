import {
  GET_ALL_USERS,
  ADD_USER,
  DELETE_USER,
  UPDATE_USER,
} from "../actions/actionTypes";

const initialState = [];
const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_USERS:
      return {
        ...state,
        users: action.users,
      };
    case ADD_USER:
      return {
        ...state,
        users: [action.user, ...state.users],
      };
    case UPDATE_USER: /////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      return {
        ...state,
        users: state.users.map((user) =>
          user.id === action.userId
            ? {
                ...user,
                name: action.user.name,
                email: action.user.email,
                password: action.user.password,
              }
            : user
        ),
      };
    case DELETE_USER:
      return {
        ...state,
        users: state.users.filter((user) => user.id !== action.userId),
      };
    default:
      return state;
  }
};

export default userReducer;
