import {
  FETCH_MESSAGES,
  ADD_MESSAGE,
  DELETE_MESSAGE,
  UPDATE_MESSAGE,
  TOGGLE_LIKE,
} from "../actions/actionTypes";

const initialState = [];
const messageReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MESSAGES:
      action.messages.forEach((mes) => (mes.isLiked = true));
      return {
        ...state,
        messages: action.messages,
      };
    case ADD_MESSAGE:
      action.message.isCanDeleted = true;
      state.messages.forEach((mes) => (mes.isCanDeleted = false));
      return {
        ...state,
        messages: [action.message, ...state.messages],
      };
    case TOGGLE_LIKE:
      return {
        ...state,
        messages: state.messages.map((mes) =>
          mes.id === action.messageId ? { ...mes, isLiked: !mes.isLiked } : mes
        ),
      };
    case UPDATE_MESSAGE:
      return {
        ...state,
        messages: state.messages.map((mes) =>
          mes.id === action.messageId
            ? {
                ...mes,
                text: action.messageBody,
              }
            : mes
        ),
      };
    case DELETE_MESSAGE:
      return {
        ...state,
        messages: state.messages.filter((mes) => mes.id !== action.messageId),
      };
    default:
      return state;
  }
};

export default messageReducer;
