import { getMessages, createMessage, delMessage, updMessage } from "../services/domainRequest/messageRequest";
//import * as conf from "./config";
import {
  ADD_MESSAGE,
  DELETE_MESSAGE,
  UPDATE_MESSAGE,
  TOGGLE_LIKE,
  FETCH_MESSAGES,
} from "./actionTypes";

const fetchMessagesAction = (messages) => ({
  type: FETCH_MESSAGES,
  messages,
});

const addMessageAction = (message) => ({
  type: ADD_MESSAGE,
  message,
});

const updateMessageAction = (messageId, messageBody) => ({
  type: UPDATE_MESSAGE,
  messageBody,
  messageId
});

const deleteMessageAction = (messageId) => ({
  type: DELETE_MESSAGE,
  messageId,
});

const toggleLikeAction = (messageId) => ({
  type: TOGGLE_LIKE,
  messageId,
});
/*******************************************************/
export const fetchMessages = () => async (dispatch) => {
  const messages = await getMessages();
  dispatch(fetchMessagesAction(messages));
};
export const addMessage = (body, user) =>async (dispatch) => {
  const newMessage = {
    userId: user.id,
    avatar: user.avatar,
    user: user.name,
    text: body,
    isLiked: true,
  };
  const mes=await createMessage(newMessage);
  dispatch(addMessageAction(mes));
};
export const deleteMessage = (messageId) => async (dispatch) => {
  await delMessage(messageId);
  dispatch(deleteMessageAction(messageId));
};

export const toggleLike = (messageId) => (dispatch) => {
  dispatch(toggleLikeAction(messageId));
};

export const updateMessage=(messageId, messageBody)=>async(dispatch)=>{
  await updMessage(messageId, messageBody);
  dispatch(updateMessageAction(messageId, messageBody.text));
};
