import {
  getUsers,
  createUser,
  delUser,
  updUser,
} from "../services/domainRequest/userRequest";
import {
  ADD_USER,
  DELETE_USER,
  UPDATE_USER,
  GET_ALL_USERS,
} from "./actionTypes";

const getAllUsersAction = (users) => ({
  type: GET_ALL_USERS,
  users,
});

const addUserAction = (user) => ({
  type: ADD_USER,
  user,
});

const updateUserAction = (userId, user) => ({
  type: UPDATE_USER,
  user,
  userId,
});

const deleteUserAction = (userId) => ({
  type: DELETE_USER,
  userId,
});

/*******************************************************/
export const getAllUsers = () => async (dispatch) => {
  const users = await getUsers();
  dispatch(getAllUsersAction(users));
};
export const addUser = (user) => async (dispatch) => {
  const newUser = await createUser(user);
  dispatch(addUserAction(newUser));
};
export const deleteUser = (userId) => async (dispatch) => {
  await delUser(userId);
  dispatch(deleteUserAction(userId));
};

export const updateUser = (userId, user) => async (dispatch) => {
  await updUser(userId, user);
  dispatch(updateUserAction(userId, user));
};
