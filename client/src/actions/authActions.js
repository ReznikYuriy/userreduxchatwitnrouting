import { login } from "../services/domainRequest/auth";
import { ADMIN_LOGGED_IN, LOGIN_SUCCESSFUL } from "./actionTypes";

const adminLoggedInAction = () => ({
  type: ADMIN_LOGGED_IN,
});

const loginSuccessfulAction = (user) => ({
  type: LOGIN_SUCCESSFUL,
  user,
});

/*******************************************************/
export const loginUser = (name, password) => async (dispatch) => {
  const data = await login({ name, password });
  if (data && !data.error) {
    if (data.role === "admin") dispatch(adminLoggedInAction());
    dispatch(loginSuccessfulAction(data));
  }
};
