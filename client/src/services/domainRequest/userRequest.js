import { get, post, deleteReq, put } from "../requestHelper";
const entity = "users";

export const createUser = async (body) => {
  return await post(entity, body);
};

export const getUsers = async () => {
  return await get(entity);
};

export const getUser = async (id) => {
  return await get(entity, id);
};

export const delUser = async (id) => {
  return await deleteReq(entity, id);
};

export const updUser = async (id, body) => {
  return await put(entity, id, body);
};
