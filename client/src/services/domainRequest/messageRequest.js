import { get, post, deleteReq, put } from "../requestHelper";

const entity = "messages";

export const getMessages = async () => {
  return await get(entity);
};

export const getMessage = async (id) => {
  return await get(entity, id);
};

export const createMessage = async (body) => {
  return await post(entity, body);
};

export const delMessage = async (id) => {
  return await deleteReq(entity, id);
};

export const updMessage = async (id, body) => {
  return await put(entity, id, body);
};
