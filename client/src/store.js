import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import messageReducer from './reducers/messageReducer';
import userReducer from './reducers/userReducer';
import authReducer from './reducers/authReducer';
import thunk from 'redux-thunk';

const initialState = {};
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const reducers = {
  messages: messageReducer,
  users: userReducer,
  auth:authReducer
};

const rootReducer = combineReducers({
  ...reducers
});
const store = createStore(
  rootReducer,
  initialState,
  composeEnhancers(applyMiddleware(thunk))
);

export default store;