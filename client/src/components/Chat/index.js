import React, { useState, useEffect } from "react";
import "./chat.css";
import Spinner from "../spinner/";
import Header from "../header";
import MessageList from "../message-list";
import MessageInput from "../message-input";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchMessages, deleteMessage, addMessage, toggleLike, updateMessage } from "../../actions/messageActions";
import {CHAT_NAME} from '../../actions/config';
import {getParticipantsCount, getLastMessageTime, getMessagesCount} from '../../actions/selectors';
import {Redirect} from 'react-router-dom';


function Chat({
  fetchMessages,
  addMessage,
  deleteMessage,
  editMessage,
  toggleLike,
  data=[],
  isLogin,
  currentUserId
}) {
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    fetchMessages().then(()=>setLoaded(true));

  }, []);

  return (
    <div className="chat">
      {!isLogin && <Redirect to="/"/>}
      {!isLoaded && <Spinner />}
      {isLoaded && (
        <>
         { <Header
            name={CHAT_NAME}
            nParticipants={getParticipantsCount(data)}
            nMessages={getMessagesCount(data)}
            lastMessage={getLastMessageTime(data)}
          />}
          <MessageList
            data={data}
            currentUserId={currentUserId}
            deleteMessage={deleteMessage}
            editMessage={editMessage}
            toggleLike={toggleLike}
          />
          <MessageInput addMessage={addMessage} />
        </>
      )}
    </div>
  );
}
const mapStateToProps = state => ({
  data: state.messages.messages,
  isLogin:state.auth.isLogin,
  currentUserId:state.auth.currentUser.id
});
const actions = {
  fetchMessages,
  deleteMessage,
  addMessage,
  updateMessage,
  toggleLike
};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
