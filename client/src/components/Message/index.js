import React from "react";
import moment from "moment";
import {withRouter} from 'react-router-dom';

import "./message.css";

const Message = ({ message, currentUserId, deleteMessage, toggleLike, history }) => {
  const { avatar, user, text, createdAt, id, userId } = message;
  const date = moment(createdAt).fromNow();

  const isMyOwnMessage = currentUserId === userId ? true : false;
  let mesLikeStyle = message.isLiked ? "up" : "down";
  let postPositionStyle = isMyOwnMessage ? "my-own-card" : "default-card";
  return (
    <div className={`${postPositionStyle} card-container`}>
      <div className="card-content">
        {!isMyOwnMessage && (
          <img className="card-img" size="mini" src={avatar} alt="avatar" />
        )}
        <div className="card-content-header">{user}</div>
        <div className="card-content-meta">posted {date}</div>
        <div className="card-description">
          <strong>{text}</strong>
        </div>
      </div>
      <div className="card-extra-content">
        <span>
          {isMyOwnMessage ? (
            <i
              className={`fa fa-thumbs-${mesLikeStyle}`}
              onClick={() => {}}
            ></i>
          ) : (
            <i
              className={`fa fa-thumbs-${mesLikeStyle}`}
              onClick={() => toggleLike(id)}
            ></i>
          )}
        </span>
        {isMyOwnMessage && (
          <span className='service-icon'>
            <i className="fa fa-pencil" onClick={()=>history.push(`/chat/${id}`)}></i>
          </span>
        )}
        {message.isCanDeleted && isMyOwnMessage && (
          <span className='service-icon'>
            <i className="fas fa-trash-alt" onClick={() => deleteMessage(id)}></i>
          </span>
        )}
      </div>
    </div>
  );
};

export default withRouter(Message);
