import { useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { bindActionCreators } from "redux";
import { addUser } from "../../actions/userActions";
import { Redirect } from "react-router-dom";
import "./add-user.css";

const AddUser = ({ addUser, isAdmin }) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const onUpdate = () => {
    if (!name || !email || !password) {
      return;
    }
    addUser({
      name: name,
      email: email,
      password: password,
      role: "user",
      avatar: "/avatar.png",
    });
    history.push("/users");
  };

  const onCancel = () => {
    setName("");
    setEmail("");
    setPassword("");
    history.push("/users");
  };

  const handleName = (e) => setName(e.target.value);
  const handleEmail = (e) => setEmail(e.target.value);
  const handlePassword = (e) => setPassword(e.target.value);

  return (
    <div className="container">
      {!isAdmin && <Redirect to="/" />}
      <div className="row">
        <h1>Add user</h1>
      </div>
      <div className="inputs">
        <div className="labels">Name</div>
        <input
          className="login-input"
          type="text"
          placeholder="Name"
          id="nameInput"
          onChange={handleName}
          value={name}
        />
        <div className="labels">Email</div>
        <input
          className="login-input"
          type="email"
          placeholder="test@mailbox.com"
          id="emailInput"
          onChange={handleEmail}
          value={email}
        />
        <div className="labels">Password</div>
        <input
          className="login-input"
          type="password"
          placeholder="**************"
          id="passwordInput"
          onChange={handlePassword}
          value={password}
        />
        <div className="buttons">
          <button onClick={onCancel} className="button-primary">
            Cancel
          </button>
          <button onClick={onUpdate} className="button-primary">
            Save user
          </button>
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = (state) => ({
  isAdmin: state.auth.isAdmin,
});
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      addUser: addUser,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(AddUser);
