import { useState } from "react";
import { connect } from "react-redux";
import { Redirect, useHistory } from "react-router-dom";
import { bindActionCreators } from "redux";
import { loginUser } from "../../actions/authActions";
import "./login.css";

const LoginPage = ({ loginUser, isAdmin, isLogin }) => {
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const onLogin = () => {
    if (!name || !password) {
      return;
    }
    loginUser(name, password);
  };

  const onCancel = () => {
    setName("");
    setPassword("");
    history.push("/");
  };

  const handleName = (e) => setName(e.target.value);
  const handlePassword = (e) => setPassword(e.target.value);
  if (isAdmin) return <Redirect to="/users" />;
  return (
    <div className="container">
      {isAdmin && <Redirect to="/users" />}
      {isLogin && <Redirect to="/chat" />}
      <div className="row">
        <h1>Login Page</h1>
      </div>
      <div className="inputs">
        
          <div className='labels'>Name</div>
          <input
            className="login-input"
            type="text"
            placeholder="Name"
            id="nameInput"
            onChange={handleName}
            value={name}
          />
          <div className='labels'>Password</div>
          <input
            className="login-input"
            type="password"
            placeholder="**************"
            id="passwordInput"
            onChange={handlePassword}
            value={password}
          />
        
        <div className="buttons">
          <button onClick={onLogin} className="button-primary">
            Login
          </button>
          <button onClick={onCancel} className="button-primary">
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = (state) => ({
  isLogin: state.auth.isLogin,
  isAdmin: state.auth.isAdmin,
});
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      loginUser: loginUser,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
