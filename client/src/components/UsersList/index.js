import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";
import {
  getAllUsers,
  deleteUser,
} from "../../actions/userActions";
import {Redirect} from 'react-router-dom';
import Spinner from "../spinner/";
import "./users-list.css";

const UsersList = ({ users, getAllUsers, deleteUser, isAdmin}) => {
  const history = useHistory();
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    getAllUsers().then(() => setLoaded(true));
  }, []);
  return (
    <div className="container-lg">
      {!isAdmin && <Redirect to="/" />}
      {!isLoaded && <Spinner />}
      {isLoaded && (
        <div className="table-responsive">
          <div className="table-wrapper">
            <div className="table-title">
              <div className="row">
                <div className="col-sm-8">
                  <h2>Users</h2>
                </div>
                <div className="col-sm-4">
                  <button type="button" className="btn btn-info add-new"
                  onClick={() => history.push(`/add-user`)}>
                    <i className="fa fa-plus"></i> Add New
                  </button>
                  <button type="button" className="btn btn-info add-new"
                  onClick={() => history.push(`/chat`)}>
                    <i className="fas fa-comment-alt"></i> Chat
                  </button>
                </div>
                
              </div>
            </div>
            <table className="table table-bordered">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>E-mail</th>
                  <th>Role</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {users.map((user) => (
                  <tr key={user.id}>
                    <td>{user.name}</td>
                    <td>{user.email}</td>
                    <td>{user.role}</td>
                    <td>
                      <span className="service-icon">
                        <i
                          className="fa fa-pencil"
                          onClick={() => history.push(`/users/${user.id}`)}
                        ></i>
                      </span>
                      <span className="service-icon">
                        <i className="fas fa-trash-alt" onClick={() => {deleteUser(user.id)}}></i>
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      )}
    </div>
  );
};
const mapStateToProps = (state) => ({
  users: state.users.users,
  isAdmin:state.auth.isAdmin
});
const actions = {
  getAllUsers,
  deleteUser,
};
const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
