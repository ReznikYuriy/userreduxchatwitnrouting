import React from 'react';
import {Route, Switch } from 'react-router-dom';
import Chat from '../Chat';
import LoginPage from '../LoginPage';
import EditMessage from '../EditMessage';
import UsersList from '../UsersList';
import EditUser from '../EditUser';
import AddUser from '../AddUser';

const App =()=>{
    return(<Switch>
        <Route path='/' component={LoginPage} exact/>
        <Route path='/chat' component={Chat} exact/>
        <Route path='/chat/:id' component={EditMessage}/>
        <Route path='/users' component={UsersList} exact/>
        <Route path='/users/:id' component={EditUser}/>
        <Route path='/add-user' component={AddUser} exact/>
    </Switch>);
}

export default App;