import React, { useState } from "react";
import {connect} from 'react-redux';
import "./message-input.css";

const MessageInput = ({ addMessage, currentUser }) => {
  const [body, setBody] = useState("");

  const handleAddMessage = (ev) => {
    ev.preventDefault();
    if (!body) {
      return;
    }
    addMessage(body, currentUser);
    setBody("");
    
  };

  return (
    <div className="message-input">
      <form onSubmit={handleAddMessage}>        
      <textarea
          name="description"
          value={body}
          placeholder="What is the news?"
          onChange={(ev) => setBody(ev.target.value)}
        />

        <button className="submit-button" type="submit">
          Post
        </button>
      </form>
    </div>
  );
};
const mapStateToProps=state=>({currentUser:state.auth.currentUser})

export default connect(mapStateToProps, null)(MessageInput);
