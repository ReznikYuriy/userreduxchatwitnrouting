import React from "react";
import "./header.css";

const Header = ({name, nParticipants, nMessages, lastMessage}) => {
  return <div className="header-container">
  <div className="header-logo">{name}</div>  
  <div>{nParticipants} participants</div>
  <div>{nMessages} messages</div>
  <div>last message {lastMessage}</div>
  </div>;
};

export default Header;
