import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators} from "redux";
import { getMessage } from "../../services/domainRequest/messageRequest";
import { updateMessage } from "../../actions/messageActions";
import { useHistory } from "react-router-dom";
import "./edit-message.css";

const EditMessage = ({ match, updateMessage }) => {
  const [body, setBody] = useState("");
  const history = useHistory();
  useEffect(() => {
    if (match.params.id) {
      getMessage(match.params.id).then((res) => setBody(res.text));
    }
  }, []);

  const onUpdate = () => {
    if (!body) {
      return;
    }
    updateMessage(match.params.id, { text: body });
    history.push("/chat");
  };

  const onCancel = () => {
    setBody("");
    history.push("/chat");
  };
  return (
    <div className="message-input">
      <form>
        <button className="submit-button" onClick={onCancel}>
          Cancel
        </button>
        <textarea
          name="description"
          value={body}
          placeholder="What is the news?"
          onChange={(ev) => setBody(ev.target.value)}
        />
        <button className="submit-button" onClick={onUpdate}>
          Update
        </button>
      </form>
    </div>
  );
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      updateMessage: updateMessage,
    },
    dispatch
  );
};


export default connect(null, mapDispatchToProps)(EditMessage);
