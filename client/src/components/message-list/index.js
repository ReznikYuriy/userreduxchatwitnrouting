import React from "react";
import "./message-list.css";
import moment from "moment";
import Message from "../Message";

const MessageList = ({ data, currentUserId, deleteMessage, toggleLike }) => {
  data.sort((a, b) => moment(b.createdAt) - moment(a.createdAt));
  const dateSet = new Set();
  data.forEach((message) => {
    dateSet.add(moment(message.createdAt).format("MMMM DD YYYY"));
  });
  const toRender = [];
  dateSet.forEach((date) => {
    toRender.push(<div key={date} className="divider-date">{date}</div>);
    data
      .filter((mes) => date === moment(mes.createdAt).format("MMMM DD YYYY"))
      .map((mes) => {
        toRender.push(
          <Message
            key={mes.id}
            message={mes}
            currentUserId={currentUserId}
            deleteMessage={deleteMessage}
            toggleLike={toggleLike}
          />
        );
      });
  });
  return <div className="message-list">{toRender}</div>;
};
export default MessageList;
