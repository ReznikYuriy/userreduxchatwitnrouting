const { Router } = require('express');
const UserService = require('../services/userService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/', (req, res, next) => {
  try {
    if (res.err) throw res.err;
    const data = req.body;
    const user = UserService.createUser(data);
    res.data = user;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/', (req, res, next) => {
  try {
    const users = UserService.getAll();
    res.data = users;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
    const id = req.params.id;
    const user = UserService.search({id});
    res.data = user;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', (req, res, next) => {
  try {
    if (res.err) throw res.err;
    const id = req.params.id;
    const updUser = UserService.updateUser(id, req.body);
    res.data = updUser;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    const id = req.params.id;
    const data = UserService.deleteUser(id);
    res.data = data;
  }
  catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;