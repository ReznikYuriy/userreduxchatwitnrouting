const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/', (req, res, next) => {
    try {
        if (res.err) throw res.err;
        const data = req.body;
        const message = MessageService.createMessage(data);
        res.data = message;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        const messages = MessageService.getAll();
        res.data = messages;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        const message = MessageService.getMessage(id);
        res.data = message;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', (req, res, next) => {
    try {
        if (res.err) throw res.err;
        const id = req.params.id;
        const data = req.body;
        res.data = MessageService.updateMessage(id, data);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        res.data = MessageService.deleteMessage(id);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;