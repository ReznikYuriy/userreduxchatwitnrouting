const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
    getAll() {
        const items = MessageRepository.getAll();
        if (!items) {
            throw new Error('Message not found');
        }
        return items;
    }

    getMessage(id) {
        const item = this.search({ id });
        if (!item) {
            throw new Error("Message not found");
        }
        return item;
    }

    createMessage(data) {
        const item = MessageRepository.create(data);
        if (!item) {
            throw new Error('Error');
        }
        return item;
    }

    search(search) {
        const item = MessageRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    updateMessage(id, dataToUpdate) {
        const _id = this.search({ id });
        if (!_id) throw new Error("Message not found");
        return MessageRepository.update(id, dataToUpdate);
    }

    deleteMessage(id) {
        const _id = this.search({ id });
        if (!_id) throw new Error("Message not found");
        return MessageRepository.delete(id);
    }
}

module.exports = new MessageService();