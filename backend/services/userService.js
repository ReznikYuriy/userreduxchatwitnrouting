const { UserRepository } = require("../repositories/userRepository");

class UserService {
   getAll() {
    const items = UserRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  createUser(data) {
    const item = UserRepository.create(data);
    if (!item) {
      return null;
    }
    return item;
  }
  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
  updateUser(id, dataToUpdate) {
    const _id = this.search({ id });
    if (!_id) throw new Error("UserNotFound");
    return UserRepository.update(id, dataToUpdate);
  }
  deleteUser(id) {
    const _id = this.search({ id });
    if (!_id) throw new Error("UserNotFound");
    return UserRepository.delete(id);
  }
}

module.exports = new UserService();
