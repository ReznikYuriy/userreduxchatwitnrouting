const { user } = require('../models/user');
const {checkToExtraField, checkToMissedField}=require('./helpers');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
        validateField(req);
        checkToMissedField(req, user);
        checkToExtraField(req, user);
        req.body['email']=req.body['email'].toLowerCase();
    } catch (err) {
        res.err = err;
    }
    finally {
        next();
    }

}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        validateField(req);
        checkToMissedField(req, user);
        checkToExtraField(req, user);
        req.body['email']=req.body['email'].toLowerCase();
    } catch (err) {
        res.err = err;
    }
    finally {
        next();
    }
}
const validateField = (req) => {
    if (!req.body.firstName || req.body.firstName === '') throw new Error("Error. First Name is empty!");
    if (!req.body.lastName || req.body.lastName === "") throw new Error("Error. Last Name is empty!");
    if (!req.body.password || req.body.password === "") throw new Error("Error. Password is empty!");
    if (String(req.body.password).length < 3) throw new Error("Error. Password have to 3 or more characters!");
    if (!(/^\+380\d{9}$/i).test(req.body.phoneNumber)) throw new Error("Error. Phone number is not valid!");
    if (!req.body.phoneNumber || req.body.phoneNumber === "") throw new Error("Error. Phone number is empty!");
    if (!req.body.email || req.body.email === "") throw new Error("Error. Email is empty!");
    if (!(/^[\w.\-]{0,25}@(gmail)\.com$/i).test(req.body.email)) throw new Error("Error. Email is not valid!");
};
/* const checkToExtraField = (req, model) => { 
    for(const field in req.body){
        if(!model.hasOwnProperty(field)||field==='id') throw new Error(`The field '${field}' is excess!`);
    }
};
const checkToMissedField = (req, model) => { 
    for(const field in model){
        if(!req.body.hasOwnProperty(field)&&field!=='id') throw new Error(`The field '${field}' is missed!`);
    }
}; */

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;