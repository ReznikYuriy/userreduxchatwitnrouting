const { fighter } = require('../models/fighter');
const {checkToExtraField, checkToMissedField}=require('./helpers');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    try {
        validateField(req);
        req.body['health']=fighter.health;
        checkToMissedField(req, fighter);
        checkToExtraField(req, fighter);
    } catch (err) {
        res.err = err;
    }
    finally {
        next();
    }

}

const updateFighterValid = (req, res, next) => {
    try {
        validateField(req);
        req.body['health']=fighter.health;
        checkToMissedField(req, fighter);
        checkToExtraField(req, fighter);
    } catch (err) {
        res.err = err;
    }
    finally {
        next();
    }
}
const validateField = (req) => {
    if (!req.body.name || req.body.name === '') throw new Error("Error. Fighter`s name is empty.");
    if (!req.body.power ||
        (typeof req.body.power != 'number' && !isFinite(req.body.power)) ||
        req.body.power < 1 ||
        req.body.power > 100) throw new Error("Error. Power is not valid or empty.");
    if (!req.body.defense ||
        (typeof req.body.defense != 'number' && !isFinite(req.body.defense)) ||
        req.body.defense < 1 ||
        req.body.defense > 10) throw new Error("Error. Defense is not valid or empty.");
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;